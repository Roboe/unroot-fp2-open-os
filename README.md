Unroot FP2 Open OS
---

Generate `zip` file:
```sh
zip -r unroot-fp2-open-os META-INF
```

Flash the resulting `unroot-fp2-open-os.zip` with TWRP recovery
